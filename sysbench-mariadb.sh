# Sysbench directory where lua test reside.
export TEST_DIR=/usr/share/sysbench
# Number of thread. Keep it to number of cores.
export THREAD=48
# How long this test suppose to run.
export TIME=60
mysqladmin -u root drop sbtest -f
mysqladmin -u root create sbtest
echo "Test(oltp_read_only):"
sysbench ${TEST_DIR}/oltp_read_only.lua --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_read_only.lua --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} run 

mysqladmin -u root drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_read_write):"
sysbench ${TEST_DIR}/oltp_read_write.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_read_write.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_write_only):"
sysbench ${TEST_DIR}/oltp_write_only.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_write_only.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(bulk_insert):"
sysbench ${TEST_DIR}/bulk_insert.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/bulk_insert.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_delete):"
sysbench ${TEST_DIR}/oltp_delete.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_delete.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_insert):"
sysbench ${TEST_DIR}/oltp_insert.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_insert.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_point_select):"
sysbench ${TEST_DIR}/oltp_point_select.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_point_select.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_update_index):"
sysbench ${TEST_DIR}/oltp_update_index.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_update_index.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(oltp_update_non_index):"
sysbench ${TEST_DIR}/oltp_update_non_index.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/oltp_update_non_index.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(select_random_points):"
sysbench ${TEST_DIR}/select_random_points.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/select_random_points.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 

mysqladmin -u root  drop sbtest -f
mysqladmin -u root  create sbtest
echo "Test(select_random_ranges):"
sysbench ${TEST_DIR}/select_random_ranges.lua  --mysql-user=root --db-driver=mysql --time=${TIME} --threads=${THREAD} prepare
sysbench ${TEST_DIR}/select_random_ranges.lua  --mysql-user=root --db-driver=mysql --time=${TIME}  --threads=${THREAD} run 
